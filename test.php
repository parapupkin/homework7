<?php
$number = ($_GET['number']);
$dir = (__DIR__ . "/tests/");
$list=scandir($dir);
if ($number < 1 || !ctype_digit($number) || $number > count($list) - 2) {
   header("HTTP/1.1 404 Not Found"); 
   exit();  
}
$test = $list[$number+1];
$content = file_get_contents($dir. $test);
$result = json_decode($content, true);
$test_name = explode(".", $test);
$test_name = $test_name[0];
$i = 0;                 
$ot = 0;
$not = 0;
$not1 = 0;
$answers = [];
$mark;  

foreach ($result as $value) {      
    foreach ($value as $questions => $answer) {       
            $answers[$questions] = $answer;
    } 
    if (empty($_POST)) {        
    } elseif (empty($_POST["q$i"])) {
            $not1++;            
    } elseif ($_POST["q$i"] == $answers["true"]) {
            $ot++;            
    } else {
            $not++;            
    }
            $i++; 
}

if ($ot == $i) {
    $mark = "Отлично!";
} elseif ($ot == $i - 1) {
    $mark = "Хорошо";
} elseif ($ot == $i - 2) {
    $mark = "Удовлетворительно";
} else {
    $mark = "Плохо!";
}

if (!empty($_POST)) {
  $text = $_POST['name']; 
  if (empty($_POST['name'])) {
       $text = "Неизвестный чел";
  } 
  $image = imagecreatetruecolor(522, 700);
  $backcolor = imagecolorallocate($image, 255, 224, 221);
  $textcolor = imagecolorallocate($image, 50, 50, 50);
  $boxFile = __DIR__ . '/image1.jpg';
  if (!file_exists($boxFile)) {
      echo "Файл с картинкой не найден";
      exit();
  }
  $fontFile = __DIR__ . '/arial.ttf';
  if (!file_exists($fontFile)) {
      echo "Файл со шрифтом не найден";
      exit();
  }
  $imBox = imagecreatefromjpeg($boxFile);
  imagecopy($image, $imBox, 0, 0, 0, 0, 522, 700);
  imagettftext($image, 30, 0, 120, 330, $textcolor, $fontFile, $text);  
  imagettftext($image, 16, 0, 260, 460, $textcolor, $fontFile, $mark);
  header('Content-type: image/jpg');
  imagejpeg($image); 
  exit();
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Test</title>
        <style type="text/css">
            body {
                line-height: 1.5;                
            }
            h1 {
                text-align: center;
                color: blue;
            }
            p {
              font-weight: bold;
            }
            .result {
                text-align: center;
                font-weight: bold;
                font-size: 20px;   
                color: red;     
            }
            
            .result:nth-last-child(3) {
                color: green;
            }
            div {
                margin: 30px auto;
                width: 500px;
            }            
        </style>
    </head>
    <body>
        <h1>Вы проходите тест <?php echo "$test_name" ?> </h1>
        <form action="" method="POST">
            <div>
                <?php                 
                $i = 0;                  
                foreach ($result as $value) {      
                    foreach ($value as $questions => $answer) {       
                            $answers[$questions] = $answer;
                   }              
                ?>                    
                <p> <?php echo $answers["question"] ?></p>                
                <label><input  name="<?php echo "q" . $i?>" type="radio" value="<?php echo $answers["answer1"] ?>"> <?php echo $answers["answer1"] ?></label><br>
                <label><input  name="<?php echo "q" . $i?>" type="radio" value="<?php echo $answers["answer2"] ?>"> <?php echo $answers["answer2"] ?></label><br>
                <label><input  name="<?php echo "q" . $i?>" type="radio" value="<?php echo $answers["answer3"] ?>"> <?php echo $answers["answer3"] ?></label><br>
                <?php
                 $i++;                                    
                }
                ?>
                <p><label> Введите свое имя и фамилию <input  name="name" type="text" value="Неизвестный"></label></p> 
                <button class="button" type="submit">Результат</button>                               
            </div>
        </form>
    </body>
</html>

